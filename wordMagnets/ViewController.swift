//
//  ViewController.swift
//  wordMagnets
//
//  Created by Jo on 12/17/18.
//  Copyright © 2018 Aleksandr Duma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = UIColor.black
        
        wordLabelCreation(wordArray: wordArrayCreation())
    }
    
    //Word array creation
    func wordArrayCreation () -> Array<String>{
        let wordArray = ["I", "like", "to", "eat", "fruit", "my", "favorite"]
        return wordArray
    }
    //Word label creation
    var labelRectangleArray: [CGRect] = []//Array of labels frames
    func wordLabelCreation (wordArray : Array<String>){
        
        for word in wordArray {
            //Label create + settings
            let label = UILabel()
            label.text = word
            label.font = UIFont.systemFont(ofSize: 34)
            label.sizeToFit()
            label.backgroundColor = UIColor.yellow
            label.isUserInteractionEnabled = true
            
            //Label center
            let x = CGFloat.random(in: (label.bounds.size.width/2.0)...(UIScreen.main.bounds.width-(label.bounds.size.width/2.0)))
            let y = CGFloat.random(in: (label.bounds.size.height/2.0)...(UIScreen.main.bounds.height-(label.bounds.size.height/2.0)))
            label.center = CGPoint(x: x, y: y)
            //labelRectangleArray.append(label.bounds.offsetBy(dx: x, dy: y)) //Adding labels as rectangles into array with center (x, y)
            //Label add to view
            view.addSubview(label)
            labelRectangleArray.append(label.frame)
            print(labelRectangleArray.last!)
            // Pan Gesture
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(ViewController.panPiece(_:)))
            label.addGestureRecognizer(panGesture)
        }
    }

    //Pan gesture recognizer
    var initialCenter = CGPoint()  // The initial center point of the view.
    @IBAction func panPiece(_ gestureRecognizer : UIPanGestureRecognizer) {
        guard gestureRecognizer.view != nil else {return}
        let piece = gestureRecognizer.view!
        // Get the changes in the X and Y directions relative to
        // the superview's coordinate space.
        let translation = gestureRecognizer.translation(in: piece.superview)
        switch gestureRecognizer.state {
            case .began:
                //Save the view's original position.
                self.initialCenter = piece.center
                
                labelRectangleArray.removeAll(where: {$0 == piece.frame})//Removing captured label from labelRectangleArray

            case .changed:
                //Add the X and Y translation to the view's original position.
                if isThereAnIntersection(piece: piece, labelRectangleArray: labelRectangleArray).0 == false{
                    let newCenter = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y + translation.y)
                    piece.center = newCenter
                }
                else {//intersection occured
                    repeat{
                        switch (isThereAnIntersection(piece: piece, labelRectangleArray: labelRectangleArray).1.origin.x,
                                isThereAnIntersection(piece: piece, labelRectangleArray: labelRectangleArray).1.origin.y){
                            
                        case (...piece.center.x, ...piece.center.y), (...piece.center.x, piece.center.y...):
                            //print("x>,y>")
                            let centerOfIntersectedObj = isThereAnIntersection(piece: piece, labelRectangleArray: labelRectangleArray).1
                            piece.center.y = centerOfIntersectedObj.midY
                            piece.center.x = centerOfIntersectedObj.maxX + (piece.frame.width / 2) + 10
                            break
                            
                        case (piece.center.x..., piece.center.y...), (piece.center.x..., ...piece.center.y):
                            //print("x<,y<")
                            let centerOfIntersectedObj = isThereAnIntersection(piece: piece, labelRectangleArray: labelRectangleArray).1
                            piece.center.y = centerOfIntersectedObj.midY
                            piece.center.x = centerOfIntersectedObj.minX - (piece.frame.width / 2) - 10
                            break
                            default:
                            print(piece.center)
                        }
                        gestureRecognizer.state = .ended
                        break
                    }
                    while isThereAnIntersection(piece: piece, labelRectangleArray: labelRectangleArray).0 == true
                    
                }
            case .ended:
                labelRectangleArray.append(piece.frame) //Adding new label position to array
            default:
            // On cancellation, return the piece to its original location.
                piece.center = initialCenter
        }
    }
    func isThereAnIntersection (piece: UIView, labelRectangleArray: [CGRect]) -> (Bool,CGRect){
        var intersectionExists: Bool = false
        var centerOfIntersectedObj: CGRect = piece.frame
        for labelRectangle in labelRectangleArray {
            if labelRectangle.intersects(piece.frame){
                intersectionExists = true
                centerOfIntersectedObj = labelRectangle
            }
        }
        
        return (intersectionExists, centerOfIntersectedObj)
    }
}

